﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplication
{
    class Match
    {
        public int Id { get; set; }
        public string CityMach { get; set; }
        public string Player1 { get; set; }
        public string Player2 { get; set; }
        public enum ResultType { Draw, Player1win, Player2win }
        public ResultType Result { get; set; }

        public Match(int id, string cityMach, string player1, string player2, ResultType result)
        {
            Id = id;
            CityMach = cityMach ?? throw new ArgumentNullException(nameof(cityMach));
            Player1 = player1 ?? throw new ArgumentNullException(nameof(player1));
            Player2 = player2 ?? throw new ArgumentNullException(nameof(player2));
            Result = result;
        }
    }
}
