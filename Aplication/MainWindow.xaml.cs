﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Aplication
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Match> games;
        List<PlayerPoints> classification;

        public MainWindow()
        {
            InitializeComponent();
            LoadClasification();
            LoadFilters();
        }

        private void LoadClasification()
        {
            using (var db = new LppDB())
            {
                #region get Games query
                var games1 = db.Games
                    .Join(db.Players,
                    g => g.Player1id,
                    p1 => p1.Id,
                    (g, p1) => new { g.Id, CityMach = g.Cityid, Player1 = p1.Name, g.Player2id, Result = g.Gameresult })
                    .Join(db.Players,
                    g => g.Player2id,
                    p2 => p2.Id,
                    (g, p2) => new { g.Id, g.CityMach, g.Player1, Player2 = p2.Name, g.Result })
                    ;

                games = games1.ToList()
                     .Join(db.Cities,
                     g => g.CityMach,
                     c => c.Id,
                     (g, c) => new
                     Match
                     (
                         g.Id,
                         c.Name,
                         g.Player1,
                         g.Player2,
                         g.Result.ToString().Equals("Player1") ? Match.ResultType.Player1win :
                         g.Result.ToString().Equals("Player2") ? Match.ResultType.Player2win :
                         Match.ResultType.Draw
                     ))
                     .ToList();
                #endregion

                #region Get classification query
                // Compute the classification taking into account wined matches as 1 point, draw as 0.5.
                const double win = 1.0, draw = 0.5;

                // Victorias
                var winPlayer1 = games
                    .Where(g => g.Result == Match.ResultType.Player1win)
                    .GroupBy(g => g.Player1)    // Agrupamos por jugador1
                    .Select(g => new { player = g.Key, points = g.Count() * win });    // Computamos las  victorias como 1 punto
                var winPlayer2 = games
                    .Where(g => g.Result == Match.ResultType.Player2win)
                    .GroupBy(g => g.Player2)
                    .Select(g => new { player = g.Key, points = g.Count() * win });
                // Empates
                var drawPlayer1 = games
                    .Where(g => g.Result == Match.ResultType.Draw)
                    .GroupBy(g => g.Player1)
                    .Select(g => new { player = g.Key, points = g.Count() * draw });  // Computamos los empates como la mitad de puntos
                var drawPlayer2 = games
                    .Where(g => g.Result == Match.ResultType.Draw)
                    .GroupBy(g => g.Player2)
                    .Select(g => new { player = g.Key, points = g.Count() * draw });
                // Lista de puntuaciones
                classification = winPlayer1
                    .Concat(winPlayer2)
                    .Concat(drawPlayer1)
                    .Concat(drawPlayer2)    // Concatenamos todos los resultados
                    .GroupBy(p => p.player) // Los agrupamos por jugador
                    .Select(p => new PlayerPoints(p.Key, p.Sum(pt => pt.points)))  // Obtenemos el jugador y la suma de todas sus puntuaciones
                    .OrderByDescending(p => p.Points)
                    .ToList();
                #endregion

                foreach (var clasif in classification)
                {
                    clasificationView.Items.Add(clasif);
                }
            }
        }

        private void LoadFilters()
        {
            using (var db = new LppDB())
            {
                var players = db.Players
                    .Select(p => p.Name);

                FilterNames.Items.Add("None");

                foreach (var player in players)
                {
                    FilterNames.Items.Add(player);
                }

                FilterNames.SelectedIndex = 0;
            }
        }

        private void FilterNames_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            #region get filtered players query
            List<PlayerPoints> filtered;
            if (FilterNames.SelectedIndex == 0)
            {
                filtered = classification;
            }
            else
            {
                filtered = classification
                    .Where(c => c.Player.Equals(FilterNames.SelectedValue))
                    .ToList();
            }
            #endregion

            clasificationView.Items.Clear();
            foreach (var filteredItem in filtered)
            {
                clasificationView.Items.Add(filteredItem);
            }
        }

        private void OpenGamesMantenance(object sender, RoutedEventArgs e)
        {
            GamesMantenance gamesMantenance = new GamesMantenance();
            gamesMantenance.Show();
        }

        private void OpenCityMantenance(object sender, RoutedEventArgs e)
        {
            CityMantenance cityMantenance = new CityMantenance();
            cityMantenance.Show();
        }

        private void CityWithMoreMaches(object sender, RoutedEventArgs e)
        {
            #region city with more maches
            // What is the city with more matches ? 
            var moreMachesCity = games
                .GroupBy(game => game.CityMach)
                .OrderByDescending(city => city.Count())
                .Select(c => c.Key)
                .First();

            QueryWindow queryWindow = new QueryWindow(new string[] { moreMachesCity });
            queryWindow.Show();
            #endregion
        }

        private void CityWithMoreDrawnMaches(object sender, RoutedEventArgs e)
        {
            #region city with more drawn maches
            // What is the city or cities with more drawn matches ? 
            var moreDrawnMatches = games
                .Where(g => g.Result.Equals(Match.ResultType.Draw))
                .GroupBy(game => game.CityMach)
                .OrderByDescending(city => city.Count())
                .Take(1)
                .First()
                .First()
                .CityMach;
            QueryWindow queryWindow = new QueryWindow(new string[] { moreDrawnMatches });
            queryWindow.Show();

            #endregion
        }

        private void PlayerWithBestWinRatio(object sender, RoutedEventArgs e)
        {
            #region player with best win ratio
            // Compute the player with best win ratio 

            // Victorias
            var winsPlayer1 = games
                .Where(g => g.Result == Match.ResultType.Player1win)
                .GroupBy(g => g.Player1)    // Agrupamos por jugador1
                .Select(g => new { player = g.Key, wins = g.Count() });

            var winsPlayer2 = games
                .Where(g => g.Result == Match.ResultType.Player2win)
                .GroupBy(g => g.Player2)    // Agrupamos por jugador2
                .Select(g => new { player = g.Key, wins = g.Count() });

            // Derrotas
            var losesPlayer1 = games
                .Where(g => g.Result == Match.ResultType.Player2win)
                .GroupBy(g => g.Player1)    // Agrupamos por jugador1
                .Select(g => new { player = g.Key, loses = g.Count() });

            var losesPlayer2 = games
                .Where(g => g.Result == Match.ResultType.Player1win)
                .GroupBy(g => g.Player2)    // Agrupamos por jugador2
                .Select(g => new { player = g.Key, loses = g.Count() });

            // Total Victorias
            var victories = winsPlayer1
                .Concat(winsPlayer2)
                // Concatenamos todos los resultados
                .GroupBy(p => p.player)     // Los agrupamos por jugador
                .Select(p => new { player = p.Key, wins = p.Sum(ply => ply.wins) });

            // Total derrotas
            var loses = losesPlayer1
                .Concat(losesPlayer2)
                // Concatenamos todos los resultados
                .GroupBy(p => p.player)     // Los agrupamos por jugador
                .Select(p => new { player = p.Key, loses = p.Sum(ply => ply.loses) });

            var ratioWinLoose = victories
                .Join(loses,
                w => w.player,
                l => l.player,
                (w, l) => new { name = w.player, ratio = (double)w.wins / (double)l.loses }); // Convertemos a double para no perder decimales

            // Consideramos el ratio como victorias/derrotas
            var bestWinRatio = ratioWinLoose
                .OrderByDescending(r => r.ratio)
                .First();

            QueryWindow queryWindow = new QueryWindow(new string[] { bestWinRatio.name + "  " + bestWinRatio.ratio });
            queryWindow.Show();
            #endregion
        }

        private void PlayerWithMoreMAchesWinned(object sender, RoutedEventArgs e)
        {
            #region player with more matches winned

            // Victorias
            var winsPlayer1 = games
                .Where(g => g.Result == Match.ResultType.Player1win)
                .GroupBy(g => g.Player1)    // Agrupamos por jugador1
                .Select(g => new { player = g.Key, wins = g.Count() });

            var winsPlayer2 = games
                .Where(g => g.Result == Match.ResultType.Player2win)
                .GroupBy(g => g.Player2)    // Agrupamos por jugador2
                .Select(g => new { player = g.Key, wins = g.Count() });
            // Total Victorias
            var victories = winsPlayer1
                .Concat(winsPlayer2)
                // Concatenamos todos los resultados
                .GroupBy(p => p.player)     // Los agrupamos por jugador
                .Select(p => new { player = p.Key, wins = p.Sum(ply => ply.wins) });
            // Compute the player with more matches winned
            var moreMachesWined = victories
                .OrderByDescending(v => v.wins)
                .First();

            QueryWindow queryWindow = new QueryWindow(new string[] { moreMachesWined.player + "  " + moreMachesWined.wins });
            queryWindow.Show();
            #endregion
        }

        private void ClassificationForEachCity(object sender, RoutedEventArgs e)
        {
            #region classification for each city
            const double win = 1.0, draw = 0.5;
            // Compute the classification for each city
            var winPlayer1City = games
                .Where(g => g.Result == Match.ResultType.Player1win)
                .Select(g => new { player = g.Player1, points = win, city = g.CityMach });

            var winPlayer2City = games
                .Where(g => g.Result == Match.ResultType.Player2win)
                .Select(g => new { player = g.Player2, points = win, city = g.CityMach });

            var drawPlayer1City = games
                .Where(g => g.Result == Match.ResultType.Draw)
                .Select(g => new { player = g.Player1, points = draw, city = g.CityMach });

            var drawPlayer2City = games
                .Where(g => g.Result == Match.ResultType.Draw)
                .Select(g => new { player = g.Player2, points = draw, city = g.CityMach });

            var listOfCityScores = winPlayer1City
                .Concat(winPlayer2City)
                .Concat(drawPlayer1City)
                .Concat(drawPlayer2City);

            var cityScores = listOfCityScores
                .GroupBy(s => new { s.city, s.player })
                .Select(c => new { city = c.Key.city, player = c.Key.player, score = c.Sum(sc => sc.points) })
                .OrderByDescending(c => c.score);

            var cityScoresCityGroups = cityScores
                .GroupBy(c => c.city);

            List<string> strlist = new List<string>();
            foreach (var city in cityScoresCityGroups)
            {
                strlist.Add(city.Key);
                strlist.Add("---------------------------------");
                foreach (var player in city)
                {
                    strlist.Add(" " + player.score.ToString().PadRight(5) + "\t" + player.player);
                }
            }

            QueryWindow queryWindow = new QueryWindow(strlist.ToArray());
            queryWindow.Show();

            #endregion
        }

        private void WorstStreak(object sender, RoutedEventArgs e)
        {
            #region worst streak
            // Compute the worst streak
            const bool wined = true, losed = false;

            var Player1Streak = games
                .Select(g => new { id = g.Id, player = g.Player1, result = g.Result == Match.ResultType.Player1win ? wined : losed });

            var Player2Streak = games
                .Select(g => new { id = g.Id, player = g.Player2, result = g.Result == Match.ResultType.Player2win ? wined : losed });

            var listOfAllGames = Player1Streak
                .Concat(Player2Streak)
                .OrderBy(l => l.id)
                .OrderBy(p => p.player)
                .ToList();  // Si no lo utilizamos como lista tarda una barbaridad en acabar

            // Variable para asignar un valor a cada strike de derrotas
            int labelStrike = 0;

            var strikes = listOfAllGames
                .Select((item, i) => new
                {
                    Item = item,
                    strike =
                    i == 0 ? labelStrike :
                    item.player == listOfAllGames.ElementAt(i - 1).player && item.result == losed ? labelStrike : ++labelStrike
                });

            var firstGroup = strikes
                .Where(sr => sr.Item.result == losed)
                .GroupBy(s => s.strike)
                .OrderByDescending(s => s.Count())
                .First();


            Console.WriteLine("Compute the worst streak:\tMade by "
                + firstGroup.First().Item.player + " and its a streak of "
                + firstGroup.Count() + " losed maches");
            #endregion

            QueryWindow queryWindow = new QueryWindow(new string[] { firstGroup.First().Item.player + "  " + firstGroup.Count() + " loses" });
            queryWindow.Show();
        }

        private void Win5OrMoreConsecutiveMatches(object sender, RoutedEventArgs e)
        {
            #region win 5 or more consecutive maches
            // Compute the player or players that has win 5 or more consecutive matches
            // Compute the worst streak
            const bool wined = true, losed = false;

            var Player1Streak = games
                .Select(g => new { id = g.Id, player = g.Player1, result = g.Result == Match.ResultType.Player1win ? wined : losed });

            var Player2Streak = games
                .Select(g => new { id = g.Id, player = g.Player2, result = g.Result == Match.ResultType.Player2win ? wined : losed });

            var listOfAllGames = Player1Streak
                .Concat(Player2Streak)
                .OrderBy(l => l.id)
                .OrderBy(p => p.player)
                .ToList();  // Si no lo utilizamos como lista tarda una barbaridad en acabar


            int labelWinStrike = 0;

            // Variable para asignar un valor a cada strike de derrotas
            int labelStrike = 0;

            var winStrikes = listOfAllGames
                .Select((item, i) => new
                {
                    Item = item,
                    strike =
                    i == 0 ? labelStrike :
                    item.player == listOfAllGames.ElementAt(i - 1).player && item.result == wined ? labelWinStrike : ++labelWinStrike
                });

            var MoreThan5MachesWinedStrike = winStrikes
                .Where(sr => sr.Item.result == wined)
                .Select(s => new { s.Item.player, s.strike })
                .GroupBy(s => s.strike)
                .Where(g => g.Count() >= 5) // Descartamos los que no hayan ganado mas de 5
                .OrderByDescending(s => s.Count());

            // Ahora debemos obtener unicamente los jugadores sin repetir que tienen strikes de mas de 5 victorias
            // ya que ahora tendremos guardados los strikes de todos los jugadores

            var MoreThan5Players = MoreThan5MachesWinedStrike
                .Select(g => g.First()); // Nos quedamos solamente con el nombre de cada jugador y su racha

            // Eliminamos las rachas del mismo jugador
            var rachasDeMasDe5 = MoreThan5Players
                .GroupBy(r => r.player)
                .Select(g => g.First());

            List<string> strlist = new List<string>();

            foreach (var r in rachasDeMasDe5)
            {
                strlist.Add(r.player);
            }

            QueryWindow queryWindow = new QueryWindow(strlist.ToArray());
            queryWindow.Show();
            #endregion
        }

        private void UnbeatenPlayerFromEachCity(object sender, RoutedEventArgs e)
        {
            #region unbeaten player of each city
            // Compute for each city the unbeaten players.
            // Compute the worst streak
            const bool wined = true, losed = false;

            var Player1Unbeaten = games
                .Select(g => new { id = g.Id, player = g.Player1, city = g.CityMach, result = g.Result == Match.ResultType.Player1win ? wined : losed });

            var Player2Unbeaten = games
                .Select(g => new { id = g.Id, player = g.Player2, city = g.CityMach, result = g.Result == Match.ResultType.Player2win ? wined : losed });

            var AllGames = Player1Unbeaten
                .Concat(Player2Unbeaten)
                .OrderBy(l => l.id)
                .OrderBy(p => p.player)
                .ToList();

            // Consultamos los jugadores que han perdido en alguna ciudad
            var losedGames = AllGames
                .Where(g => g.result == losed)
                .Select(g => new { g.player, g.city });

            //Eliminamos los jugadores que han sido derrotados en una ciudad concreta
            AllGames.RemoveAll(g => losedGames.Contains(new { g.player, g.city }));

            var playersByCity = AllGames
                .Select(p => new { p.player, p.city })
                .Distinct()
                .GroupBy(g => g.city);


            List<string> strlist = new List<string>();

            foreach (var city in playersByCity)
            {
                strlist.Add("Unbeaten players in " + city.Key);
                foreach (var pl in city)
                {
                    strlist.Add(" -" + pl.player);
                }
            }

            QueryWindow queryWindow = new QueryWindow(strlist.ToArray());
            queryWindow.Show();
            #endregion
        }
    }

    /// <summary>
    /// Clase para encapsulacion de la classificacion de los jugadores
    /// </summary>
    class PlayerPoints
    {
        public string Player { get; set; }
        public double Points { get; set; }

        public PlayerPoints(string player, double points)
        {
            Player = player ?? throw new ArgumentNullException(nameof(player));
            Points = points;
        }
    }

}
