//---------------------------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated by T4Model template for T4 (https://github.com/linq2db/t4models).
//    Changes to this file may cause incorrect behavior and will be lost if the code is regenerated.
// </auto-generated>
//---------------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;

using LinqToDB;
using LinqToDB.Mapping;

namespace Aplication
{
	/// <summary>
	/// Database       : lpp
	/// Data Source    : tcp://localhost:5432
	/// Server Version : 11.1
	/// </summary>
	public partial class LppDB : LinqToDB.Data.DataConnection
	{
		public ITable<City>   Cities  { get { return this.GetTable<City>(); } }
		public ITable<Game>   Games   { get { return this.GetTable<Game>(); } }
		public ITable<Player> Players { get { return this.GetTable<Player>(); } }

		public LppDB()
		{
			InitDataContext();
		}

		public LppDB(string configuration)
			: base(configuration)
		{
			InitDataContext();
		}

		partial void InitDataContext();
	}

	[Table(Schema="public", Name="cities")]
	public partial class City
	{
		[Column(@"id"),   PrimaryKey, NotNull] public int    Id   { get; set; } // integer
		[Column(@"name"),             NotNull] public string Name { get; set; } // text

		#region Associations

		/// <summary>
		/// games_cityid_fkey_BackReference
		/// </summary>
		[Association(ThisKey="Id", OtherKey="Cityid", CanBeNull=true, Relationship=Relationship.OneToMany, IsBackReference=true)]
		public IEnumerable<Game> gamescityidfkeys { get; set; }

		#endregion
	}

	[Table(Schema="public", Name="games")]
	public partial class Game
	{
		[Column(@"id"),         PrimaryKey,  NotNull] public int    Id         { get; set; } // integer
		[Column(@"cityid"),        Nullable         ] public int?   Cityid     { get; set; } // integer
		[Column(@"player1id"),     Nullable         ] public int?   Player1id  { get; set; } // integer
		[Column(@"player2id"),     Nullable         ] public int?   Player2id  { get; set; } // integer
		[Column(@"gameresult"),              NotNull] public object Gameresult { get; set; } // USER-DEFINED

		#region Associations

		/// <summary>
		/// games_cityid_fkey
		/// </summary>
		[Association(ThisKey="Cityid", OtherKey="Id", CanBeNull=true, Relationship=Relationship.ManyToOne, KeyName="games_cityid_fkey", BackReferenceName="gamescityidfkeys")]
		public City cityidfkey { get; set; }

		/// <summary>
		/// games_player1id_fkey
		/// </summary>
		[Association(ThisKey="Player1id", OtherKey="Id", CanBeNull=true, Relationship=Relationship.ManyToOne, KeyName="games_player1id_fkey", BackReferenceName="gamesplayer1idfkey")]
		public Player player1idfkey { get; set; }

		/// <summary>
		/// games_player2id_fkey
		/// </summary>
		[Association(ThisKey="Player2id", OtherKey="Id", CanBeNull=true, Relationship=Relationship.ManyToOne, KeyName="games_player2id_fkey", BackReferenceName="gamesplayer2idfkey")]
		public Player player2idfkey { get; set; }

		#endregion
	}

	[Table(Schema="public", Name="players")]
	public partial class Player
	{
		[Column(@"id"),   PrimaryKey, NotNull] public int    Id   { get; set; } // integer
		[Column(@"name"),             NotNull] public string Name { get; set; } // text

		#region Associations

		/// <summary>
		/// games_player1id_fkey_BackReference
		/// </summary>
		[Association(ThisKey="Id", OtherKey="Player1id", CanBeNull=true, Relationship=Relationship.OneToMany, IsBackReference=true)]
		public IEnumerable<Game> gamesplayer1idfkey { get; set; }

		/// <summary>
		/// games_player2id_fkey_BackReference
		/// </summary>
		[Association(ThisKey="Id", OtherKey="Player2id", CanBeNull=true, Relationship=Relationship.OneToMany, IsBackReference=true)]
		public IEnumerable<Game> gamesplayer2idfkey { get; set; }

		#endregion
	}

	public static partial class TableExtensions
	{
		public static City Find(this ITable<City> table, int Id)
		{
			return table.FirstOrDefault(t =>
				t.Id == Id);
		}

		public static Game Find(this ITable<Game> table, int Id)
		{
			return table.FirstOrDefault(t =>
				t.Id == Id);
		}

		public static Player Find(this ITable<Player> table, int Id)
		{
			return table.FirstOrDefault(t =>
				t.Id == Id);
		}
	}
}
