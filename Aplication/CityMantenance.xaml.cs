﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Aplication
{
    /// <summary>
    /// Lógica de interacción para CityMantenance.xaml
    /// </summary>
    public partial class CityMantenance : Window
    {
        public CityMantenance()
        {
            InitializeComponent();
            LoadCities();
        }

        private void LoadCities()
        {
            using (var db = new LppDB())
            {
                var cities = db.Cities
                    .OrderBy(c => c.Id);

                vistaCiudades.Items.Clear();
                foreach (var city in cities)
                {
                    vistaCiudades.Items.Add(city);
                }
            }
        }

        private void EditCity(object sender, RoutedEventArgs e)
        {
            if (vistaCiudades.SelectedIndex < 0)
            {
                // No ha seleccionado nada asi que no editamos
                MessageBox.Show("Selecciona un elemento de la lista", "Valor no seleccionado");
                return;
            }

            using (var db = new LppDB())
            {
                int selectedId = ((City)vistaCiudades.SelectedItem).Id;
                string name = EditName.Text;
                if (name.Length <= 0 || name == null)
                {
                    MessageBox.Show("El nombre debe contener almenos 1 caracter", "Valor no introducido");
                }

                using (NpgsqlConnection connection = new NpgsqlConnection(db.ConnectionString))
                {
                    connection.Open();

                    NpgsqlCommand cmd = new NpgsqlCommand(
                        "UPDATE cities SET " +
                        "name = '" + name + "' " +
                        "WHERE id = " + selectedId + ";",
                        connection);
                    NpgsqlDataReader dr = cmd.ExecuteReader();
                    dr.Close();

                }
            }

            LoadCities();
        }

        private void AddCity(object sender, RoutedEventArgs e)
        {
            using (var db = new LppDB())
            {
                var lastId = db.Cities
                    .Max(c => c.Id)
                    + 1;

                var name = EditName.Text;
                if (name.Length <= 0 || name == null)
                {
                    MessageBox.Show("El nombre debe contener almenos 1 caracter", "Valor no introducido");
                    return;
                }

                using (NpgsqlConnection connection = new NpgsqlConnection(db.ConnectionString))
                {
                    connection.Open();
                    NpgsqlCommand cmd = new NpgsqlCommand(
                        "INSERT INTO cities (id, name) " +
                        "VALUES (" + lastId + ",'" + name + "');",
                        connection);
                    NpgsqlDataReader dr = cmd.ExecuteReader();
                    dr.Close();
                }
            }
            LoadCities();
        }

        private void DeleteCity(object sender, RoutedEventArgs e)
        {
            if (vistaCiudades.SelectedIndex < 0)
            {
                // No ha seleccionado nada asi que no editamos
                MessageBox.Show("Selecciona un elemento de la lista", "Valor no seleccionado");
                return;
            }

            using (var db = new LppDB())
            {
                int selectedId = ((City)vistaCiudades.SelectedItem).Id;

                using (NpgsqlConnection connection = new NpgsqlConnection(db.ConnectionString))
                {
                    connection.Open();
                    NpgsqlCommand cmd = new NpgsqlCommand(
                        "DELETE FROM cities WHERE " +
                        "id = " + selectedId + ";",
                        connection);
                    NpgsqlDataReader dr = cmd.ExecuteReader();
                    dr.Close();
                }
            }
            LoadCities();
        }

        private void SelectedCity(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                EditName.Text = ((City)vistaCiudades.SelectedItem).Name;
            }
            catch (NullReferenceException)
            {
                EditName.Text = "";
            }
        }
    }
}
