﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Aplication
{
    /// <summary>
    /// Lógica de interacción para GamesMantenance.xaml
    /// </summary>
    public partial class GamesMantenance : Window
    {
        List<Match> games;

        public GamesMantenance()
        {
            InitializeComponent();
            LoadGames();
            LoadFilters();
            LoadEditors();
        }

        private void LoadGames()
        {
            using (var db = new LppDB())
            {
                #region get Games query
                games = db.Games
                    .Join(db.Players,
                    g => g.Player1id,
                    p1 => p1.Id,
                    (g, p1) => new { g.Id, CityMach = g.Cityid, Player1 = p1.Name, g.Player2id, Result = g.Gameresult })
                    .Join(db.Players,
                    g => g.Player2id,
                    p2 => p2.Id,
                    (g, p2) => new { g.Id, g.CityMach, g.Player1, Player2 = p2.Name, g.Result })
                    .ToList()
                     .Join(db.Cities,
                     g => g.CityMach,
                     c => c.Id,
                     (g, c) => new
                     Match
                     (
                         g.Id,
                         c.Name,
                         g.Player1,
                         g.Player2,
                         g.Result.ToString().Equals("Player1") ? Match.ResultType.Player1win :
                         g.Result.ToString().Equals("Player2") ? Match.ResultType.Player2win :
                         Match.ResultType.Draw
                     ))
                     .OrderBy(g => g.Id)
                     .ToList();
                #endregion
                GamesView.Items.Clear();
                foreach (var game in games)
                {
                    GamesView.Items.Add(game);
                }
            }
        }

        private void LoadFilters()
        {
            using (var db = new LppDB())
            {
                var players = db.Players
                    .Select(p => p.Name);

                FilterNames1.Items.Add("None");
                FilterNames2.Items.Add("None");

                foreach (var player in players)
                {
                    FilterNames1.Items.Add(player);
                    FilterNames2.Items.Add(player);
                }

                FilterNames1.SelectedIndex = 0;
                FilterNames2.SelectedIndex = 0;
            }
        }

        private void LoadEditors()
        {
            using (var db = new LppDB())
            {
                var cities = db.Cities
                    .Select(p => p.Name);

                var players = db.Players
                    .Select(p => p.Name);

                foreach (var city in cities)
                {
                    CityEdit.Items.Add(city);
                }

                foreach (var player in players)
                {
                    Player1Edit.Items.Add(player);
                    Player2Edit.Items.Add(player);
                }
                ResultEdit.Items.Add(Match.ResultType.Player1win);
                ResultEdit.Items.Add(Match.ResultType.Player2win);
                ResultEdit.Items.Add(Match.ResultType.Draw);
            }
        }

        private void FilterGames()
        {
            #region get filtered players query
            List<Match> filtered;
            if (FilterNames1.SelectedIndex == 0)
            {
                filtered = games;
            }
            else
            {
                filtered = games
                    .Where(c => c.Player1.Equals(FilterNames1.SelectedValue))
                    .ToList();
            }
            if (FilterNames2.SelectedIndex != 0)
            {
                filtered = filtered
                    .Where(c => c.Player2.Equals(FilterNames2.SelectedValue))
                    .ToList();
            }
            #endregion

            GamesView.Items.Clear();
            foreach (var filteredItem in filtered)
            {
                GamesView.Items.Add(filteredItem);
            }
        }

        private void FilterPlayer1ChangedValue(object sender, SelectionChangedEventArgs e)
        {
            FilterGames();
        }

        private void FilterPlayer2ChangedValue(object sender, SelectionChangedEventArgs e)
        {
            FilterGames();
        }

        private void GamesView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Match selected = (Match)GamesView.SelectedItem;

                CityEdit.SelectedItem = selected.CityMach;
                Player1Edit.SelectedItem = selected.Player1;
                Player2Edit.SelectedItem = selected.Player2;
                ResultEdit.SelectedItem = selected.Result;
            }
            catch (NullReferenceException) { }
        }

        private void AddGame(object sender, RoutedEventArgs e)
        {
            if (CityEdit.SelectedIndex < 0 ||
                Player1Edit.SelectedIndex < 0 ||
                Player2Edit.SelectedIndex < 0 ||
                ResultEdit.SelectedIndex < 0)
            {
                // No ha seleccionado nada asi que no editamos
                MessageBox.Show("Rellena todos los atributos del nuevo elemento", "Faltan valores");
                return;
            }
            using (var db = new LppDB())
            {
                var lastId = db.Games
                    .Max(g => g.Id)
                    + 1;
                int cityid = CityEdit.SelectedIndex + 1;
                int player1id = Player1Edit.SelectedIndex + 1;
                int player2id = Player2Edit.SelectedIndex + 1;
                string gameresult = ResultEdit.SelectedItem.ToString() == "Player1win" ? "Player1" :
                    ResultEdit.SelectedItem.ToString() == "Player2win" ? "Player2" :
                    "Draw";

                using (NpgsqlConnection connection = new NpgsqlConnection(db.ConnectionString))
                {
                    connection.Open();

                    NpgsqlCommand cmd = new NpgsqlCommand(
                        "INSERT INTO games ( id, cityid, player1id, player2id, gameresult) " +
                        "VALUES (" + lastId + "," + cityid + "," + player1id + "," + player2id + ",'" + gameresult + "');",
                        connection);
                    NpgsqlDataReader dr = cmd.ExecuteReader();
                    dr.Close();
                }
            }
            // Actualizamos lista y filtros para mostrar el nuevo valor
            LoadGames();
            FilterGames();
        }

        private void ModifyGame(object sender, RoutedEventArgs e)
        {
            if (GamesView.SelectedIndex < 0)
            {
                // No ha seleccionado nada asi que no editamos
                MessageBox.Show("Selecciona un elemento de la lista", "Valor no seleccionado");
                return;
            }
            using (var db = new LppDB())
            {
                int selectedId = ((Match)GamesView.SelectedItem).Id;
                int cityid = CityEdit.SelectedIndex + 1;
                int player1id = Player1Edit.SelectedIndex + 1;
                int player2id = Player2Edit.SelectedIndex + 1;
                string gameresult = ResultEdit.SelectedItem.ToString() == "Player1win" ? "Player1" :
                    ResultEdit.SelectedItem.ToString() == "Player2win" ? "Player2" :
                    "Draw";

                using (NpgsqlConnection connection = new NpgsqlConnection(db.ConnectionString))
                {
                    connection.Open();

                    NpgsqlCommand cmd = new NpgsqlCommand(
                        "UPDATE games SET " +
                        "cityid = " + cityid + ", " +
                        "player1id = " + player1id + ", " +
                        "player2id = " + player2id + ", " +
                        "gameresult = '" + gameresult + "' " +
                        "where id = " + selectedId + ";",
                        connection);
                    NpgsqlDataReader dr = cmd.ExecuteReader();
                    dr.Close();

                }
            }
            // Actualizamos lista y filtros para mostrar el nuevo valor
            LoadGames();
            FilterGames();
        }

        private void DeleteGame(object sender, RoutedEventArgs e)
        {
            if (GamesView.SelectedIndex < 0)
            {
                // No ha seleccionado nada asi que no editamos
                MessageBox.Show("Selecciona un elemento de la lista", "Valor no seleccionado");
                return;
            }

            int selectedId = ((Match)GamesView.SelectedItem).Id;

            using (var db = new LppDB())
            {

                using (NpgsqlConnection connection = new NpgsqlConnection(db.ConnectionString))
                {
                    connection.Open();

                    NpgsqlCommand cmd = new NpgsqlCommand(
                        "DELETE FROM games WHERE id = " + selectedId + "; ",
                        connection);
                    NpgsqlDataReader dr = cmd.ExecuteReader();
                    dr.Close();

                }
            }

            // Actualizamos lista y filtros para mostrar el nuevo valor
            LoadGames();
            FilterGames();
        }
    }
}