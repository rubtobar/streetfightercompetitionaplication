﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplication
{
    class City
    {
        public string Name { get; set; }

        public City(string name)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
        }

        /// <summary>
        /// Obtiene un objeto diccionario con los 
        /// objetos City mapeados con su id
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static Dictionary<int, City> ReadCSV(string path)
        {
            Dictionary<int, City> readedCities = new Dictionary<int, City>();

            // Read the csv
            var reader = new System.IO.StreamReader(path);

            // discard header names
            reader.ReadLine();

            string line;
            while (!reader.EndOfStream)
            {
                line = reader.ReadLine();
                string[] values = line.Split(';');

                // Create dictionary entry with key=ID and value=City
                readedCities.Add(int.Parse(values[0]), new City(values[1]));
            }

            reader.Close();

            return readedCities;
        }
    }
}
