﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Aplication
{
    /// <summary>
    /// Lógica de interacción para queryWindow.xaml
    /// </summary>
    public partial class QueryWindow : Window
    {
        public QueryWindow(string[] resut)
        {
            InitializeComponent();

            foreach (var item in resut)
            {
                queryView.Items.Add(item);
            }
        }
    }
}
